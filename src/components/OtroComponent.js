import { movies } from "../data/movies";
import Movie from "./Movie";
import './movies.css';

const fetchMovieData = () =>{
    return movies;
}
export function loadHotjar(apiKey) {
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:3743376,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    console.log('termina la carga')
}
export function deleteHotjar() {
    const scriptElements = document.querySelectorAll('script[async][src*="hotjar"]');

    if (window.hj && typeof window.hj === 'function') {
        window.hj('event', 'HotjarStop');
        window.hj('heatmaps', false);
        window.hj('recordings', false);
    }

    scriptElements.forEach(scriptElement => {
        scriptElement.parentNode.removeChild(scriptElement);
    });
} 


const OtroComponent =()=>{
    const MovieData = fetchMovieData();
    return (
        <div className="movie-container">
            <h2>ruta que si debe grabar</h2>
            <ul className="movie-list">
                {

                    MovieData.map((movie)=>(
                        
                        <Movie key={movie.id} movie={movie} />
                    ))
                }

            </ul>
        </div>
    )
};

export default OtroComponent;