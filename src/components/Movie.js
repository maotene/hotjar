import { loadHotjar } from "./movieComponent";
import { deleteHotjar } from "./movieComponent";
const Movie = ({movie}) => {
    const handleClick = (data) => {
        console.log(data)
        if(data==='INICIAR HOTJAR'){
            loadHotjar();
        }else {
            deleteHotjar();
        }
        // implementation details
      };

    return (
        <li className="movie" key={movie.id}>
            <img src={movie.poster} alt={movie.title}/>
            <p className="description">
                {movie.title}
            </p>
            <button type="button" onClick={()=> handleClick(movie.title)}>HOTJAR</button>

        </li>
    )
}

export default Movie;