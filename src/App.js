// Importa los componentes necesarios
import React from 'react';
import './App.css';

import MovieComponent from './components/movieComponent';
import OtroComponent from './components/OtroComponent';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
        <Route path="/" element={<MovieComponent />} />
          <Route path="/about" element={<OtroComponent />} />
          </Routes>
      </div>
    </Router>
  );
}

export default App;
